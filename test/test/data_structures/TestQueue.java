package test.data_structures;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Queue;

public class TestQueue 
{
	private Queue<String> queue;

	@Before
	public void setUp1() 
	{
		queue= new Queue<String>();
	}

	public void setUp2() 
	{
		for(int i =0; i< 10; i++)
		{
			queue.enqueue(""+i);
		}
	}

	@Test
	public void testDequeue() 
	{
		setUp2();
		assertEquals("0", queue.getFirst());
		queue.dequeue();
		assertEquals("1", queue.getFirst());
	}

	@Test
	public void testEnqueue() 
	{
		setUp1();
		queue.enqueue("100");
		assertEquals("100", queue.getFirst());
		queue.enqueue("10");
		assertEquals("100", queue.getFirst());
	}
	@Test
	public void testgetSize() 
	{
		setUp2();
		assertEquals(10, queue.getSize());
	}

}
