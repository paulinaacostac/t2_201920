package test.data_structures;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Stack;


public class TestStack 
{
	private Stack<String> stack;

	@Before
	public void setUp1() 
	{
		stack= new Stack<String>();
	}

	public void setUp2() 
	{
		for(int i =0; i< 10; i++)
		{
			stack.push(""+i);
		}
	}

	@Test
	public void testPop() 
	{
		setUp2();
		assertEquals("9", stack.getTop());
	}

	@Test
	public void testPush() 
	{
		setUp1();
		stack.push("100");
		assertEquals("100", stack.getTop());
		stack.push("10");
		assertEquals("10", stack.getTop());
	}
	@Test
	public void testgetSize() 
	{
		setUp2();
		assertEquals(10, stack.getSize());
	}
}
