package model.data_structures;

/**
 * @author leone
 * @version 1.0
 */
public class Queue<T> implements IQueue<T> {

	private Node<T> first;
	private Node<T> last;
	private int size;

	public Queue()
	{
		size = 0;
		first = null;
		last = null;
	}

	/**
	 * 
	 * @param pItem
	 */
	public void enqueue(T pItem)
	{
		Node<T> old = last;
		last = new Node<T>(pItem);
		if (isEmpty()) 
			first = last;
		else
			old.cambiarSiguiente(last);
		size++;
	}

	public T dequeue()
	{
		T eliminado = null;
		if (first != null) 
		{
			eliminado = first.darElemento();
			first = first.darSiguiente();
			size--;
		} 
		if(isEmpty())
			last = null;
		return eliminado;
	}

	public int getSize(){
		return size;

	}

	public boolean isEmpty()
	{
		return size==0;
	}

	public T getFirst()
	{
		return first.darElemento();
	}

}//end Queue