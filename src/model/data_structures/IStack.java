package model.data_structures;


/**
 * @author leone
 * @version 1.0
 * @created 27-ago.-2019 09:06:44 p.m.
 */
public interface IStack<T> {

	public void push(T pItem);

	public T pop();

	public int getSize();

	public boolean isEmpty();

	public T getTop();

}