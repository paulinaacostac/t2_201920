
package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo<T>
{
	/**
	 * Atributos del modelo del mundo
	 */
	private Queue<Viaje> colaViajes;
	private Stack<Viaje> pilaViajes;

	public MVCModelo()
	{
		colaViajes=new Queue<Viaje>();
		pilaViajes=new Stack<Viaje>();
		
	}
	public void cargar()
	{
		CSVReader reader = null;
		try 
		{

			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));
			reader.skip(1);
			for(String[] nextLine : reader) 
			{
				Viaje viaje = new Viaje(Double.parseDouble(nextLine[0]), Double.parseDouble(nextLine[1]), Double.parseDouble(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6])); 
				colaViajes.enqueue(viaje);
				pilaViajes.push(viaje);
				//System.out.println("col1: " + nextLine[0] + ", col2: "+ nextLine[1s]+ ", col3: "+ nextLine[2]+ ", col4: "+ nextLine[3]+ ", col5: "+ nextLine[4]+ ", col6: "+ nextLine[5]+ ", col7: "+ nextLine[6]);
			}
		}

			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			} 
			catch (IOException e)
			{
				e.printStackTrace();
			}
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	public String darTamano()
	{
		return String.valueOf(colaViajes.getSize());
	}

	
		public void darColaPorHora(String pHora)
		{
			Queue<Viaje> colaNueva=new Queue<Viaje>();
			Integer hora=Integer.parseInt(pHora);
			while(!colaViajes.isEmpty())
			{
				if(hora.compareTo(Integer.parseInt(colaViajes.getFirst().darHod()))<=0)
				{
					colaNueva.enqueue(colaViajes.getFirst());
					colaViajes.dequeue();
				}
				else
				{
					colaViajes.dequeue();
				}
		
			}
			colaViajes=colaNueva;
		}

		public Queue<Viaje> crearCluster(String pHora) 
		{
			Viaje ant=null;
			boolean stop=false;
			Queue<Viaje> colaNueva=new Queue<Viaje>();
			while(!stop)
			{
				ant=colaViajes.getFirst();
				colaViajes.dequeue();
				if (Integer.parseInt(ant.darHod())<=Integer.parseInt(colaViajes.getFirst().darHod()))
				{
					colaNueva.enqueue(ant);
				}
				else
				{
					colaNueva.enqueue(ant);
					stop=true;
				}
				
			}
			return colaNueva;
			
		}
		public Queue<Viaje> maximoCola(String pHora)
		{
			Queue<Viaje> colaMaxima=new Queue<Viaje>();
			Queue<Viaje> colaNueva=new Queue<Viaje>();
			while(!colaViajes.isEmpty())
			{
				colaMaxima=crearCluster(pHora);
				colaNueva=crearCluster(pHora);
				if (colaNueva.getSize()>colaMaxima.getSize())
				{
					colaMaxima=colaNueva;
				}
			}
			return colaMaxima;
		}
		public String maximoColaString(String pHora)
		{
			String r="";
			Queue<Viaje> cola=maximoCola(pHora);
			while(!cola.isEmpty())
			{
				r+=cola.getFirst().darSourceid()+cola.getFirst().darDstid()+cola.getFirst().darHod()+cola.getFirst().darMeanTraveltTime()+cola.getFirst().darStandardDeviationTravelTime()+cola.getFirst().darGeometricMeanTravelTime()+cola.getFirst().darGeometricStandardDeviationTravelTime();
				cola.dequeue();
			}
			return r;
		}

		public void darPilaPorHora(String pHora) 
		{
			Integer hora=Integer.parseInt(pHora);
			Stack<Viaje> pilaNueva=new Stack<Viaje>();
			while(!pilaViajes.isEmpty())
			{
				if (hora.compareTo(Integer.parseInt(pilaViajes.getTop().darHod()))==0)
				{
					pilaNueva.push(pilaViajes.getTop());
					pilaViajes.pop();
				}
			}
			pilaViajes=pilaNueva;
		}
		public Stack<Viaje> procesarPila(String pHora, String pN)
		{
			int N=Integer.parseInt(pN);
			darPilaPorHora(pHora);
			while(!(pilaViajes.getSize()== N))
			{
				pilaViajes.pop();
			}
			return pilaViajes;
		}
		public String procesarPilaString(String pHora, String pN)
		{
			String r="";
			Stack<Viaje> pila=procesarPila(pHora, pN);
			while(!pila.isEmpty())
			{
				r+=pila.getTop().darSourceid()+pila.getTop().darDstid()+pila.getTop().darHod()+pila.getTop().darMeanTraveltTime()+pila.getTop().darStandardDeviationTravelTime()+pila.getTop().darGeometricMeanTravelTime()+pila.getTop().darGeometricStandardDeviationTravelTime();
				pila.pop();
			}
			return r;
		}


	}
