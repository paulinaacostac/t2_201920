package model.logic;

public class Viaje implements Comparable<Viaje>
{
	private double sourceid;
	private double dstid;
	private double hod;
	private double meantraveltime;
	private double standarddeviationtraveltime;
	private double geometricmeantraveltime;
	private double geometricstandarddeviationtraveltime;
	
	public Viaje (double psourceid, double pdstid, double pHod,double pmeantraveltime, double pstandarddeviationtraveltime, double pgeometricmeantraveltime, double pgeometricstandarddeviationtraveltime )
	{
		sourceid=psourceid;
		dstid=pdstid;
		hod=pHod;
		meantraveltime=pmeantraveltime;
		standarddeviationtraveltime=pstandarddeviationtraveltime;
		geometricmeantraveltime=pgeometricmeantraveltime;
		geometricstandarddeviationtraveltime=pgeometricstandarddeviationtraveltime;
		
	}
	public String darSourceid()
	{
		return String.valueOf(sourceid);
	}
	public String darDstid()
	{
		return String.valueOf(dstid);
	}
	public String darHod()
	{
		return String.valueOf(hod);
	}
	public String darMeanTraveltTime()
	{
		return String.valueOf(meantraveltime);
	}
	public String darStandardDeviationTravelTime()
	{
		return String.valueOf(standarddeviationtraveltime);
	}
	public String darGeometricMeanTravelTime()
	{
		return String.valueOf(geometricmeantraveltime);
	}
	public String darGeometricStandardDeviationTravelTime()
	{
		return String.valueOf(geometricstandarddeviationtraveltime);
	}
	public int compareTo(Viaje o) 
	{
		return darHod().compareTo(o.darHod());
	}



}
