package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String dato2 = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("--------- \nCargando... ");
				    //int capacidad = lector.nextInt();
				    modelo = new MVCModelo(); 
				    modelo.cargar();
					System.out.println("Archivo CSV cargado");
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;

				case 2:
					System.out.println("--------- \nDar hora: ");
					dato = lector.next();
					respuesta =modelo.maximoColaString(dato);
					System.out.println("Viajes agrupados:");
					System.out.println(dato);
					break;

				case 3:
					System.out.println("--------- \nDar hora: ");
					dato = lector.next();
					System.out.println("--------- \nDar numero ultimos viajes a reportar: ");
					dato2 = lector.next();
					respuesta = modelo.procesarPilaString(dato,dato2);
					if ( respuesta != null)
					{
						System.out.println(respuesta);
					}
					else
					{
						System.out.println("Dato NO encontrado");
					}
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;

				case 4:
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	
				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
